console.log("Mabuhay!");

// JS - is a loosely type Programming Language

alert("Hello Again");

console. log( "Hello World!");

console.
log
(
	"Hello World!"
)
;

// [Section] - Making Comments in JS
// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/*
	1. Single-Line comment - ctrl + /
	2. Multi-Line comment - ctrl + shift + /
*/

// [Section] - Variables
// used to contain data
// usually stored in a computer's memory.

// Declaration of variable

// Syntax --> let variableName;
let myVariable;

console.log(myVariable);

// Syntax --> let variableName = variableValue;
let mySecondVariable = 2;

console.log(mySecondVariable);

let productName = "desktop computer";
console.log(productName);

// Reassigning value
let friend;

friend = "Kate";
friend = "Jane";
friend = "Arnel"
console.log(friend);

// Syntax const variableName variableValue

const pet = "Bruno";
// pet = "Lala";
console.log (pet);

// Local and Global Variables

let outerVariable = "hello"; //This is a global variable

{ 
	let innerVariable = "Hello World!"; //Local variable
	console.log(innerVariable);
}

console.log(outerVariable);

const outerExample = "Global Variable";

{
	const innerExample = "Local Variable"
	console.log(innerExample);
}

console.log(outerExample);

// Multiple Declaration

let productCode = "DC017", productBrand = "Dell";

/*let productCode = "DC017";
let productBrand = "Dell";*/

console.log(productCode, productBrand);

//[SECTION] Data Type
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote

let country ='Philippines'
let province ="Metro Manila"

// Concatenating Strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ", " + country;
console.log(fullAddress)

let greeting = "I live in" + ", " + country;
console.log(greeting);

// The escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// Using the double quotes along with the single quotes can allow us to easily include single quotes in texts without using the escape character

let message = "John's employees went home early";
message = 'John\'s employees went home early';
message = 'John\'s employees went \"home early\"';
console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers or Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining strings and integers/numbers
console.log("My grade last sem is" + grade);

// Boolean
// Boolean values are normally used to stroe values relating to the state of certain things

let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that is used to stroe multiple values
// Arrays can store different data types but is normally used to store similar data types
// SYNTAX: let/const arrayName = [elementA, elementB, elementC, ...]

// Array with same data types
let grades = [98.7, 92.1, 90.2, 94.6]
console.log(grades);

// Array with different data types
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that is used to mimic real world objects/items
// SYNTAX
					/* let/const objectName = {
							propertyA: value,
							propertyB: value
						}
					*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contacts: ["09123456789", "09999999999"],
	address: {  //nested object
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

// Re-assigning value to an array
const anime = ["one piece", "one punch man", "Attack on Titan"];
anime[0] = "Kimetsu no Yaiba";

console.log(anime);